---
source: sections/_api_builder_forms_upload/onEnter.md
title: onEnter
bookmark: on-enter
description: Try to open the file browse dialog when the enter key is pressed. Currently this only works if the focus is set to the browse button using the mouse.
endpoint: method
signature: true
---

#### Signature
##### **onEnter**(): *boolean*
{: .signature }

#### Returns
Returns `true` to prevent further key bubbling.
