---
source: sections/_api_builder_forms_upload/quota.md
title: quota
bookmark: quota
description: Specifies a total maximum which can be occupied by the upload control.
endpoint: method
signature: true
---

#### Signature
##### **quota**(max: *number*, unit: *"B" | "kB" | "MB" | "GB" | "TB"*): *this*
{: .signature }

`max` number
: Specifies the maximum size.

`unit` "B" | "kB" | "MB" | "GB" | "TB"

#### Returns
Returns a reference to the control to allow chaining.
