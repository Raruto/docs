---
source: sections/_api_builder_forms_upload/placeholder.md
title: placeholder
bookmark: placeholder
description: Sets a placeholder for the upload control.
endpoint: method
signature: true
---

#### Signature
##### **placeholder**(placeholder: *string*): *this*
{: .signature }

`placeholder` string
: Specifies the placeholder text.

#### Returns
Returns a reference to the control to allow chaining.
