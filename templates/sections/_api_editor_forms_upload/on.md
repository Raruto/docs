---
source: sections/_api_builder_forms_upload/on.md
title: "on"
bookmark: "on"
description: Specifies the function which is invoked when the upload is changed.
endpoint: method
signature: true
---

#### Signature
##### **on**(change: *function*): *this*
{: .signature }

`change` function
: Specifies the change function. The `Upload` instance is supplied as argument to the function.

#### Returns
Returns a reference to the control to allow chaining.
