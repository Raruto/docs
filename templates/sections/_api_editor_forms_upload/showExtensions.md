---
source: sections/_api_builder_forms_upload/showExtensions.md
title: showExtensions
bookmark: show-extensions
description: Retrieves if extensions are shown.
endpoint: property
readonly: true
signature: true
---

#### Signature
##### **showExtensions**: *boolean*
{: .signature }
