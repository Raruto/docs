---
source: sections/_api_builder_forms_upload/dragDrop.md
title: dragDrop
bookmark: drag-drop
description: Specifies if file drag and drop is allowed.
endpoint: method
signature: true
---

#### Signature
##### **dragDrop**(dragDrop?: *boolean*): *this*
{: .signature }

`dragDrop` Optional boolean
: Specifies if drag-and-drop is enabled (defaults to `true` if omitted).

#### Returns
Returns a reference to the control to allow chaining.
