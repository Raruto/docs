---
source: sections/_api_builder_forms_upload/isUploading.md
title: isUploading
bookmark: is-uploading
description: Retrieves if the control is uploading.
endpoint: property
readonly: true
signature: true
---

#### Signature
##### **isUploading**: *boolean*
{: .signature }
