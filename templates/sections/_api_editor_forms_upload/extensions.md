---
source: sections/_api_builder_forms_upload/extensions.md
title: extensions
bookmark: extensions
description: Specifies if extensions should be displayed.
endpoint: method
signature: true
---

#### Signature
##### **extensions**(extensions?: *boolean*): *this*
{: .signature }

`extensions` Optional boolean
: Specifies if extensions are displayed (defaults to `true` if omitted).

#### Returns
Returns a reference to the control to allow chaining.
