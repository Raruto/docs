---
source: sections/_api_builder_forms_upload/onListReady.md
title: onListReady
bookmark: on-list-ready
description: Invokes when the upload list is ready.
endpoint: method
signature: true
---

#### Signature
##### **onListReady**(): *void*
{: .signature }

#### Returns
Nothing
