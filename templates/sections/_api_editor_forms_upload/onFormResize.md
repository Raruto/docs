---
source: sections/_api_builder_forms_upload/onFormResize.md
title: onFormResize
bookmark: on-form-resize
description: Invoked when the form is resized.
endpoint: method
signature: true
---

#### Signature
##### **onFormResize**(): *void*
{: .signature }

#### Returns
Nothing
