---
source: sections/_api_builder_forms_upload/accept.md
title: accept
bookmark: accept
description: Specifies the file types which are accepted by the server.
endpoint: method
signature: true
---

#### Signature
##### **accept**(accept: *string[] | "images" | "video" | "audio" | "pdf"*): *this*
{: .signature }

`accept` string[] | "images" | "video" | "audio" | "pdf"
: Specifies the types that are accepted by the server. This can be one of the standard templates (`image`, `video`, `audio` or `pdf`) or can be a custom list (array) of types. each type should be one of the following specifier strings:
- A file extension starting with the STOP character (U+002E). (e.g. `.jpg`, `.png`, `.doc`);
- A valid MIME type with no extensions (e.g. `application/pdf`);
- `image/*` representing image files;
- `video/*` representing video files;
- `audio/*` representing sound files.

#### Returns
Returns a reference to the control to allow chaining.
