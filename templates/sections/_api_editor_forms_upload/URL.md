---
source: sections/_api_builder_forms_upload/URL.md
title: URL
bookmark: url
description: Sets the upload URL.
endpoint: method
signature: true
---

#### Signature
##### **URL**(URL: *string*): *this*
{: .signature }

`URL` string
: Specifies the upload URL.

#### Returns
Returns a reference to the control to allow chaining.
