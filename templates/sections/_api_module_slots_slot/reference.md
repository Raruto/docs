---
source: sections/_api_module_slots_slot/reference.md
title: reference
bookmark: reference
description: Retrieves the slot reference.
endpoint: property
readonly: true
signature: true
---

#### Signature
##### **reference**(): *string*
{: .signature }
