---
source: sections/_api_module_slots_slot/sequence.md
title: sequence
bookmark: sequence
description: Retrieves or specifies the sequence number.
endpoint: property
signature: true
---

#### Signature
##### **sequence**(): *number*
