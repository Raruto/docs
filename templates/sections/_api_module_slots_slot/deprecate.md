---
source: sections/_api_module_slots_slot/deprecate.md
title: deprecate
bookmark: deprecate
description: Deprecates a slot.
endpoint: method
signature: true
---

#### Signature
##### **deprecate**(): *this*
{: .signature }

#### Returns
Returns a reference to the instance.
