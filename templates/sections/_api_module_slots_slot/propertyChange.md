---
source: sections/_api_module_slots_slot/propertyChange.md
title: propertyChange
bookmark: property-change
description: Emits the `OnSlotChange` event.
endpoint: method
signature: true
---

#### Signature
##### **propertyChange**(property: *keyof this*): *void*
{: .signature }

`property` keyof this
: Specifies the changed property.

#### Returns
Nothing
