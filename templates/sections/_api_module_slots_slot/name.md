---
source: sections/_api_module_slots_slot/name.md
title: name
bookmark: name
description: Contains the name of the slot.
endpoint: property
signature: true
---

#### Signature
##### **name**?: *string*
{: .signature }
