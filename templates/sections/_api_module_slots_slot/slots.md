---
source: sections/_api_module_slots_slot/slots.md
title: slots
bookmark: slots
description: Retrieves a reference to the slots.
endpoint: property
readonly: true
signature: true
---

#### Signature
##### **slots**(): *Slots | undefined*
{: .signature }
