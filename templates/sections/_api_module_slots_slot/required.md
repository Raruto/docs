---
source: sections/_api_module_slots_slot/required.md
title: required
bookmark: required
description: Contains if this slot is required.
endpoint: property
signature: true
---

#### Signature
##### **required**?: *boolean*
{: .signature }
