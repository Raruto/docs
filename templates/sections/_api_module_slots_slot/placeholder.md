---
source: sections/_api_module_slots_slot/placeholder.md
title: placeholder
bookmark: placeholder
description: Contains the placeholder of the slot.
endpoint: property
signature: true
---

#### Signature
##### **placeholder**?: *string*
{: .signature }
