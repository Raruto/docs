---
source: sections/_api_builder_forms_numeric/string.md
title: string
bookmark: string
description: Retrieves the formatted value.
endpoint: property
readonly: true
signature: true
---

#### Signature
##### **string**: *string*
{: .signature }

