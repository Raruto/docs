---
source: sections/_api_builder_forms_numeric/min.md
title: min
bookmark: min
description: Sets a minimum value.
endpoint: method
signature: true
---

#### Signature
##### **min**(min: *number*): *this*
{: .signature }

`min` number
: Specifies the minimum value.

#### Returns
Returns a reference to the control to allow chaining.
