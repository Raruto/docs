---
source: sections/_api_builder_forms_numeric/enter.md
title: enter
bookmark: enter
description: Specifies the function which is invoked when the user presses the enter key.
endpoint: method
signature: true
---

#### Signature
##### **enter**(enter: *function*): *this*
{: .signature }

`enter` function
: Specifies the function to invoke. To cancel the normal behavior return `true` within the function. The `Numeric` instance is supplied as argument to the function.

#### Returns
Returns a reference to the control to allow chaining.
