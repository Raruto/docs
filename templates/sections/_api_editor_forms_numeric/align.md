---
source: sections/_api_builder_forms_numeric/align.md
title: align
bookmark: align
description: Sets the alignment of the control.
endpoint: method
signature: true
---

#### Signature
##### **align**(align: *"left" | "center" | "right"*): *this*
{: .signature }

`align` "left" | "center" | "right"
: Specifies the alignment.

#### Returns
Returns a reference to the control to allow chaining.
