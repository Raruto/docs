---
source: sections/_api_builder_forms_numeric/thousands.md
title: thousands
bookmark: thousands
description: Specifies if the thousands separator should be shown.
endpoint: method
signature: true
---

#### Signature
##### **thousands**(thousands: *boolean*): *this*
{: .signature }

`thousands` boolean
: Specifies if the thousands separator should be shown.

#### Returns
Returns a reference to the control to allow chaining.
