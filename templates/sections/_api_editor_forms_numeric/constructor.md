---
source: sections/_api_builder_forms_numeric/constructor.md
title: constructor
bookmark: constructor
description: Creates a new number instance.
endpoint: constructor
signature: true
---

#### Signature
##### new Forms.**Numeric**(value?: *TBinding\<number\>*, style?: *INumericStyle*): *Numeric*
{: .signature }

`value` Optional TBinding\<number\>
: Specifies the initial value (defaults to `0` if omitted).

`style` Optional INumericStyle
: Specifies the optional style.

#### Returns
`Numeric`
