---
source: sections/_api_builder_templates_collection/deleteUnnamed.md
title: deleteUnnamed
bookmark: delete-unnamed
description: Deletes all unnamed items from the collection.
endpoint: method
signature: true
---

#### Signature
##### **deleteUnnamed**(): *void*
{: .signature }

#### Returns
Nothing
