---
source: sections/_api_builder_templates_collection/onNameChange.md
title: onNameChange
bookmark: on-name-change
description: Invoked when the name of an item is changed.
endpoint: method
signature: true
---

#### Signature
##### **onNameChange**(item: *T*): *void*
{: .signature }

`item` T
: Reference to the item which name is changed.

#### Returns
Nothing
