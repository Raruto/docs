---
source: sections/_api_builder_forms_checkbox/toggle.md
title: toggle
bookmark: toggle
description: Toggles the checkbox.
endpoint: method
signature: true
---

#### Signature
##### **toggle**(): *this*
{: .signature }

#### Returns
Returns a reference to the control to allow chaining.
