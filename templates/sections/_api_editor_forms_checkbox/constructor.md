---
source: sections/_api_builder_forms_button/constructor.md
title: constructor
bookmark: constructor
description: Creates a new checkbox instance.
endpoint: constructor
signature: true
---

#### Signature
##### new Forms.**Checkbox**(label: *string*, checked?: *TBinding\<boolean\>*, style?: *ICheckboxStyle*): *Checkbox*
{: .signature }

`label` string
: Specifies the label.

`checked` TBinding\<boolean\>
: Specifies if the checkbox is checked (defaults to `false` if omitted).

`style` Optional ICheckboxStyle
: Specifies the optional style.

#### Returns
A new `Checkbox` instance.
