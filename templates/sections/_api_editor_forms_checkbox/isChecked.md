---
source: sections/_api_builder_forms_checkbox/isChecked.md
title: isChecked
bookmark: is-checked
description: Retrieves if the checkbox is checked, or sets the checkbox state.
endpoint: property
signature: true
---

#### Signature
##### **isChecked**: *boolean*
{: .signature }
