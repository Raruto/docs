---
source: sections/_api_builder_forms_checkbox/blur.md
title: blur
bookmark: blur
description: Blurs the focus of the checkbox.
endpoint: method
signature: true
---

#### Signature
##### **blur**(): *void*
{: .signature }

#### Returns
Nothing
