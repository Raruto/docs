---
source: sections/_api_builder_forms_static/constructor.md
title: constructor
bookmark: constructor
description: Creates a new static instance.
endpoint: constructor
signature: true
---

#### Signature
##### new Forms.**Static**(label: *string*, style?: *IStaticStyle*): *Static*
{: .signature }

`label` string
: Specifies the label.

`style` Optional IStaticStyle
: Specifies the optional style.

#### Returns
`Static`
