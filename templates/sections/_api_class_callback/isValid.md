---
source: sections/_api_class_callback/isValid.md
title: isValid
bookmark: is-valid
description: Verifies if the callback pointer is valid.
endpoint: property
readonly: true
signature: true
---

#### Signature
##### **isValid**: *boolean*
{: .signature }
