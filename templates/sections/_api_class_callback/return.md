---
source: sections/_api_class_callback/return.md
title: return
bookmark: return
description: Sets the return payload of the callback function.
endpoint: method
signature: true
---

#### Signature
##### **return**(payload: *T | undefined*): *T | undefined*
{: .signature }

#### Returns
`T | undefined`
