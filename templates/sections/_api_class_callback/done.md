---
source: sections/_api_class_callback/done.md
title: done
bookmark: done
description: Terminates the callback.
endpoint: method
signature: true
---

#### Signature
##### **done**(): *boolean*
{: .signature }

#### Returns
Returns `true` if the callback loop is terminated.
