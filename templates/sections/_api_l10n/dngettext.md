---
source: sections/_api_l10n/dngettext.md
title: dngettext
bookmark: dngettext
description: Translates a plural string using the specified language domain.
endpoint: function
signature: true
---

#### Signature
##### **dngettext**(domain: *string*, id: *string*, pluralId: *string*, count: *number*, ...arguments: *string[]*): *string*
{: .signature }

`domain` string
: Specifies the language domain identifier.

`id` string
: Specifies the message identifier or string to translate.

`pluralId` string
: Specifies the plural message identifier or string to translate.

`count` number
: Specifies the count for the plural.

`...arguments` string[]
: Optional string arguments which can be referenced in the translation string using the percent sign followed by the argument index `%n`. The count value `nCount` is automatically included as the first argument (`%1`).

#### Returns
Returns the translated string.
