---
source: sections/_api_l10n/gettext.md
title: gettext
bookmark: gettext
description: Translates a string using the default language.
endpoint: function
signature: true
---

#### Signature
##### **gettext**(id: *string*, ...arguments: *string[]*): *string*
{: .signature }

`id` string
: Specifies the message identifier or string to translate.

`...arguments` string[]
: Optional string arguments which can be referenced in the translation string using the percent sign followed by the argument index `%n`. The first argument is referenced with `%1`.

#### Returns
Returns the translated string.
