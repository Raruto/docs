---
source: sections/_api_l10n/dgettext.md
title: dgettext
bookmark: dgettext
description: Translates a string using the specified language domain.
endpoint: function
signature: true
---

#### Signature
##### **dgettext**(domain: *string*, id: *string*, ...arguments: *string[]*): *string*
{: .signature }

`domain` string
: Specifies the language domain identifier.

`id` string
: Specifies the message identifier or string to translate.

`...arguments` string[]
: Optional string arguments which can be referenced in the translation string using the percent sign followed by the argument index `%n`. The first argument is referenced with `%1`.

#### Returns
Returns the translated string.
