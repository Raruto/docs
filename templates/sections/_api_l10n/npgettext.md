---
source: sections/_api_l10n/npgettext.md
title: npgettext
bookmark: npgettext
description: Translates a plural string using the default language.
endpoint: function
signature: true
---

#### Signature
##### **npgettext**(context: *string*, id: *string*, pluralId: *string*, count: *number*, ...arguments: *string[]*): *string*
{: .signature }

`context` string
: Specifies the translation context.

`id` string
: Specifies the message identifier or string to translate.

`pluralId` string
: Specifies the plural message identifier or string to translate.

`count` number
: Specifies the count for the plural.

`...arguments` string[]
: Optional string arguments which can be referenced in the translation string using the percent sign followed by the argument index `%n`. The count value `nCount` is automatically included as the first argument (`%1`).

#### Returns
Returns the translated string.
