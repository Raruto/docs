---
source: sections/_api_l10n/pgettext.md
title: pgettext
bookmark: pgettext
description: Translates a string from a specific context using the default language.
endpoint: function
signature: true
---

#### Signature
##### **pgettext**(context: *string*, id: *string*, ...arguments: *string[]*): *string*
{: .signature }

`context` string
: Specifies the translation context.

`id` string
: Specifies the message identifier or string to translate.

`...arguments` string[]
: Optional string arguments which can be referenced in the translation string using the percent sign followed by the argument index `%n`. The first argument is referenced with `%1`.

#### Returns
Returns the translated string.
