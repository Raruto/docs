---
source: sections/_api_builder_forms_button/constructor.md
title: constructor
bookmark: constructor
description: Creates a new button instance.
endpoint: constructor
signature: true
---

#### Signature
##### new Forms.**Button**(label: *string*, type?: *Types*, style?: *IButtonStyle*): *Button*
{: .signature }

`label` string
: Specifies the label.

`type` Optional Types
: Specifies the type (defaults to `normal` if omitted). Types:
- `normal`
- `accept`
- `warning`
- `cancel`

`style` Optional IButtonStyle
: Specifies the optional style.

#### Returns
A new `Button` instance.
