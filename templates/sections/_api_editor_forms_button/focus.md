---
source: sections/_api_builder_forms_button/focus.md
title: focus
bookmark: focus
description: Sets the focus to the control.
endpoint: method
signature: true
---

#### Signature
##### **focus**(): *void*
{: .signature }

#### Returns
Returns `true` if the focus is set.
