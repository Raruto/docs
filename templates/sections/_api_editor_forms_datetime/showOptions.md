---
source: sections/_api_builder_forms_datetime/showOptions.md
title: showOptions
bookmark: show_options
description: Shows the options menu.
endpoint: method
signature: true
---

#### Signature
##### **showOptions**(): *void*
{: .signature }

#### Returns
Nothing
