---
source: sections/_api_builder_forms_datetime/minute.md
title: minute
bookmark: minute
description: Retrieves or specifies the minute.
endpoint: property
signature: true
---

#### Signature
##### **minute**: *number*
{: .signature }
