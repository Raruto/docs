---
source: sections/_api_builder_forms_datetime/showYearSelector.md
title: showYearSelector
bookmark: show_year_selector
description: Shows the year selector.
endpoint: method
signature: true
---

#### Signature
##### **showYearSelector**(): *void*
{: .signature }

#### Returns
Nothing
