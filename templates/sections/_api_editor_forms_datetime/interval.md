---
source: sections/_api_builder_forms_datetime/interval.md
title: interval
bookmark: interval
description: Specifies the minute/second interval.
endpoint: method
signature: true
---

#### Signature
##### **interval**(interval: *number*): *this*
{: .signature }

`interval` number
: Specifies the interval.

#### Returns
Returns a reference to the control to allow chaining.
