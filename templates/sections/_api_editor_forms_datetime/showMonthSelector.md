---
source: sections/_api_builder_forms_datetime/showMonthSelector.md
title: showMonthSelector
bookmark: show_month_selector
description: Shows the month selector.
endpoint: method
signature: true
---

#### Signature
##### **showMonthSelector**(): *void*
{: .signature }

#### Returns
Nothing
