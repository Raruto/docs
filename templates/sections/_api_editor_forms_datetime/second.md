---
source: sections/_api_builder_forms_datetime/second.md
title: second
bookmark: second
description: Retrieves or specifies the second.
endpoint: property
signature: true
---

#### Signature
##### **second**: *number*
{: .signature }

