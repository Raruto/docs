---
source: sections/_api_builder_forms_datetime/constructor.md
title: constructor
bookmark: constructor
description: Creates a new date instance.
endpoint: constructor
signature: true
---

#### Signature
##### new Forms.**DateTime**(value?: *TBinding\<number\>*, style?: *IDateTimeStyle*): *DateTime*
{: .signature }

`value` Optional TBinding\<number\>
: Specifies the initial value.

`style` Optional IDateTimeStyle
: Specifies the optional style.
