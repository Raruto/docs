---
source: sections/_api_builder_forms_datetime/on.md
title: "on"
bookmark: "on"
description: Specifies the function which is invoked when the date is changed.
endpoint: method
signature: true
---

#### Signature
##### **on**(change: *function*): *this*
{: .signature }

`change` function
: Specifies the change function. The `DateTime` instance is supplied as argument to the function.

#### Returns
Returns a reference to the control to allow chaining.
