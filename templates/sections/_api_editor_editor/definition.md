---
source: sections/_api_builder_builder/definition.md
title: definition
bookmark: definition
description: Retrieves or specifies the form definition.
endpoint: property
signature: true
---

#### Signature
##### **definition**: *IDefinition | undefined*
{: .signature }
