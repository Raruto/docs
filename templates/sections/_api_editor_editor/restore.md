---
source: sections/_api_builder_builder/restore.md
title: restore
bookmark: restore
description: Restores the last loaded or saved definition.
endpoint: method
signature: true
---

#### Signature
##### **restore**(): *this*
