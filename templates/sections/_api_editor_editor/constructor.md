---
source: sections/_api_builder_builder/constructor.md
title: constructor
bookmark: constructor
description: Creates a new builder instance.
endpoint: constructor
signature: true
---

#### Signature
##### new **Builder**(properties?: *[IBuilderProperties](../ibuilderproperties)*): *Builder*
{: .signature }

`properties` Optional [IBuilderProperties](../ibuilderproperties)
: Specifies the [builder properties](../ibuilderproperties).

#### Returns
A new `Builder` instance.
