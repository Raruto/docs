---
source: sections/_api_builder_builder/onLoad.md
title: onLoad
bookmark: on-load
description: Invoked when the builder is loaded.
endpoint: method
signature: true
---

#### Signature
##### **onLoad**(): *(pBuilder: Builder) => void*
{: .signature }
