---
source: sections/_api_builder_builder/load.md
title: load
bookmark: load
description: Loads a form definition.
endpoint: method
signature: true
---

#### Signature
##### **load**(definition: *IDefinition*): *this*

`definition` IDefinition
: Specifies the form definition.
