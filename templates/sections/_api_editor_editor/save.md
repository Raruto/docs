---
source: sections/_api_builder_builder/save.md
title: save
bookmark: save
description: Saves the form definition.
endpoint: method
signature: true
---

#### Signature
##### **save**(): *IDefinition | undefined*
