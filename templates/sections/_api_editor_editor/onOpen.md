---
source: sections/_api_builder_builder/onOpen.md
title: onOpen
bookmark: on-open
description: Invoked when the builder is opened.
endpoint: method
signature: true
---

#### Signature
##### **onOpen**(): *(pBuilder: Builder) => void*
{: .signature }
