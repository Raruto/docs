---
source: sections/_api_builder_builder/tutorial.md
title: tutorial
bookmark: tutorial
description: Shows the tutorial dialog.
endpoint: method
signature: true
---

#### Signature
##### **tutorial**(): *this*
