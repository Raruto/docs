---
source: sections/_api_builder_builder/open.md
title: open
bookmark: open
description: Opens the builder.
endpoint: method
signature: true
---

#### Signature
##### **open**(definition?: *IDefinition*): *this*

`definition` Optional IDefinition
: Optionally specifies the form definition to load.
