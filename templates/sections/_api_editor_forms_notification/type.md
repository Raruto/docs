---
source: sections/_api_builder_forms_notification/type.md
title: type
bookmark: type
description: Changes the notification type.
endpoint: method
signature: true
---

#### Signature
##### **type**(type: *Types*): *this*
{: .signature }

`type` Types
: Specifies the type.

#### Returns
Returns a reference to the control to allow chaining.
