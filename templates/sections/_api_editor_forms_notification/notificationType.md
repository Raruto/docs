---
source: sections/_api_builder_forms_notification/notificationType.md
title: notificationType
bookmark: notificationType
description: Retrieves or specifies the notification type.
endpoint: property
signature: true
---

#### Signature
##### **notificationType**: *Types*
{: .signature }
