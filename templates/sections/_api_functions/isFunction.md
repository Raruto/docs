---
source: sections/_api_functions/isFunction.md
title: isFunction
bookmark: is-function
description: Validates if the supplied variable is a function.
endpoint: function
signature: true
code: |
  ``` typescript
  isFunction(() => {}); // Returns `true`
  ```
---

#### Signature
##### **isFunction**(func: *any*): *boolean*
{: .signature }

`func` any
: Variable to validate.

#### Returns
Returns `true` if the variable is a function.
