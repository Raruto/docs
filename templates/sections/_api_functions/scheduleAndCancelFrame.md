---
source: sections/_api_functions/scheduleAndCancelFrame.md
title: scheduleAndCancelFrame
bookmark: schedule-and-cancel-frame
description: Calls the supplied function asynchronous on the next frame.
endpoint: function
signature: true
  ```
---

#### Signature
##### **scheduleAndCancelFrame**(callee: *function | undefined*, handle: *number*, ...arguments: *any[]*): *number*
{: .signature }

`callee` function | undefined
: Specifies the function to execute.

`handle` number
: Specifies the handle of the call to cancel.

`...arguments` any[]
: Optional additional arguments which will be passed to the callee.

#### Returns
Returns the call handle.
