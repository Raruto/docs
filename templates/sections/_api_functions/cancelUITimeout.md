---
source: sections/_api_functions/cancelUITimeout.md
title: cancelUITimeout
bookmark: cancel-ui-timeout
description: Cancels a pending UI timeout call.
endpoint: function
signature: true
---

#### Signature
##### **cancelUITimeout**(handle: *number*): *number*
{: .signature }

`handle` number
: Contains the handle of the asynchronous timeout call.

#### Returns
Always returns 0.
