---
source: sections/_api_functions/scheduleInterval.md
title: scheduleInterval
bookmark: schedule-interval
description: Schedules the supplied function and invokes it on the specified interval (using `setInterval`).
endpoint: function
signature: true
---

#### Signature
##### **scheduleInterval**(callee: *function*, interval: *number*, ...arguments: *any[]*): *number*
{: .signature }

`callee` function
: Specifies the function to execute.

`interval` number
: Specifies the interval in milliseconds.

`...arguments` any[]
: Optional additional arguments which will be passed to the callee.

#### Returns
Returns the call handle.
