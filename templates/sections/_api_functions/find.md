---
source: sections/_api_functions/find.md
title: find
bookmark: find
description: Finds the first item from an array, collection or enumerable object that matches the truth function and returns its key.
endpoint: function
signature: true
code: |
  ``` typescript
  find<number>([1, 2, 3], (nItem: number) => nItem > 1); // Returns `1`
  find<Object>({a: 1, b: 2, c: 3}, (nItem: number) => nItem > 1); // Returns `b`
  ```
---

#### Signature
##### **find**\<T\>(list: *TList\<T\> | undefined*, truth: *function*): *string | number | undefined*
{: .signature }

`list` TList\<T\> | undefined
: Specifies the array, collection or enumerable object to iterate through.

`truth` function
: Specifies the optional truth function. Each item is supplied to the function, passing the item as argument. The function should return a boolean value whether to include the item in the selection. If omitted all items match.

#### Returns
Returns the item key or `undefined` if no item is found.
