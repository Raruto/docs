---
source: sections/_api_functions/reduce.md
title: reduce
bookmark: reduce
description: Reduce boils down an array, collection or enumerable object into a single value of type R.
endpoint: function
signature: true
code: |
  ``` typescript
  reduce<number, number>([1, 2, 3], (nValue: number, nItem: number) => nValue + nItem, 2); // Returns `8`
  ```
---

#### Signature
##### **reduce**\<T, R\>(list: *TList\<T\> | undefined*, reduce: *function*, initial?: *R*, ...arguments: *any[]*): *R | undefined*
{: .signature }

`list` TList\<T\> | undefined
: Specifies the array, collection or enumerable object to iterate through.

`reduce` function
: Specifies the reduce function to be invoked for each element. The return value of each reduce function will be the current reduced value.

`initial` Optional R
: Optional initial value for the reduce value.

`...arguments` any[]
: Optional additional arguments which will be passed to the callee.

#### Returns
Returns the reduced value.
