---
source: sections/_api_functions/isFilledString.md
title: isFilledString
bookmark: is-filled-string
description: Validates if the supplied variable is a string which is not empty.
endpoint: function
signature: true
code: |
  ``` typescript
  isFilledString("ABC"); // Returns `true`
  isFilledString(""); // Returns `false`
  isFilledString(1); // Returns `false`
  ```
---

#### Signature
##### **isFilledString**(str: *any*): *boolean*
{: .signature }

`str` any
: Variable to validate.

#### Returns
Returns `true` if the variable is a string with content.
