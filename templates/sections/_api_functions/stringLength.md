---
source: sections/_api_functions/stringLength.md
title: stringLength
bookmark: string-length
description: Returns the length of the supplied string.
endpoint: function
signature: true
code: |
  ``` typescript
  stringLength("ABC"); // Returns `3`
  stringLength(1); // Returns `0`
  ```
---

#### Signature
##### **stringLength**(str: *any*): *number*
{: .signature }

`str` any
: Specifies the string.

#### Returns
Returns the length of the string or `0` if the supplied variable is not a valid string or an empty string.
