---
source: sections/_api_functions/findFirst.md
title: findFirst
bookmark: find-first
description: Selects the first item from an array, collection or enumerable object that matches the truth function.
endpoint: function
signature: true
code: |
  ``` typescript
  findFirst<number>([1, 2, 3], (nItem: number) => nItem > 1); // Returns `2`
  ```
---

#### Signature
##### **findFirst**\<T\>(list: *TList\<T\> | undefined*, truth: *function*): *T | undefined*
{: .signature }

`list` TList\<T\> | undefined
: Specifies the array, collection or enumerable object to iterate through.

`truth` function
: Specifies the optional truth function. Each item is supplied to the function, passing the item as argument. The function should return a boolean value whether to include the item in the selection. If omitted all items match.

#### Returns
Returns the item or `undefined` if no item is found.
