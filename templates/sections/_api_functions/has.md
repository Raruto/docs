---
source: sections/_api_functions/has.md
title: has
bookmark: has
description: Verifies an array, collection or enumerable object by iterating through the items and verify them against the supplied truth function.
endpoint: function
signature: true
code: |
  ``` typescript
  has<number>([1, 2, 3], (nItem: number) => nItem > 0); // Returns `true`
  has<number>([1, 2, 3], (nItem: number) => nItem > 1); // Returns `false`
  has<number>([1, 2, 3], (nItem: number) => nItem > 1, true); // Returns `true`
  ```
---

#### Signature
##### **has**\<T\>(list: *TList\<T\> | undefined*, truth: *function*, oneOrMore?: *boolean*): *boolean*
{: .signature }

`list` TList\<T\> | undefined
: Specifies the array, collection or enumerable object to iterate through.

`truth` function
: Specifies the truth function. Each item is supplied to the function, passing the item as argument. The function should return a boolean value with the individual verification result.

`oneOrMore` Optional boolean
: Optional parameter which specifies if one or more items should pass the verification. (defaults to `false` if omitted). By default (`false`) all items should pass (equals `HasOnly`).

#### Returns
Returns `true` in case of a verification pass.
