---
source: sections/_api_functions/scheduleAnimation.md
title: scheduleAnimation
bookmark: schedule-animation
description: Calls the supplied animation function using a double animation frame request. Use this function if you need to be sure all pending DOM reads and writes are performed.
endpoint: function
signature: true
---

#### Signature
##### **scheduleAnimation**(animation: *function | undefined*, ...arguments: *any[]*): *void*
{: .signature }

`animation` function | undefined
: Specifies the function to execute.

`...arguments` any[]
: Optional additional arguments which will be passed to the callee.

#### Returns
Nothing
