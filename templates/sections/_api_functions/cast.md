---
source: sections/_api_functions/cast.md
title: cast
bookmark: cast
description: Cast variable to the desired type without type checking.
endpoint: function
signature: true
---

#### Signature
##### **cast**\<To\>(source: *any*): *To*
{: .signature }

`source` any
: Specifies the source.

#### Returns
Returns the desired type.
