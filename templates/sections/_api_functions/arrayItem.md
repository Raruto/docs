---
source: sections/_api_functions/arrayItem.md
title: arrayItem
bookmark: array-item
description: Returns the item at the specified index in the array, collection or enumerable object.
endpoint: function
signature: true
code: |
  ``` typescript
  arrayItem<number>([1, 2, 3], 2); // Returns `3`
  arrayItem<number>([1, 2, 3], 3); // Returns `undefined`
  arrayItem<number>([1, 2, 3], 3, 0); // Returns `0`
  ```
---

#### Signature
##### **arrayItem**\<T\>(array: *TList\<T\> | undefined*, index: *number*, default?: *T*): *T | undefined*
{: .signature }

`array` TList\<T\> | undefined
: Specifies the array, collection or enumerable object.

`index` Number
: Specifies the zero based item index.

`default` Optional T
: Optional parameter which specifies the default value if the supplied index is invalid.

#### Returns
Returns the item at the specified index.

