---
source: sections/_api_functions/firstArrayItem.md
title: firstArrayItem
bookmark: first-array-item
description: Returns the first item in the array, collection or enumerable object.
endpoint: function
signature: true
code: |
  ``` typescript
  firstArrayItem<number>([1, 2, 3]); // Returns `1`
  ```
---

#### Signature
##### **firstArrayItem**\<T\>(array: *TList\<T\> | undefined*, default?: *T*): *T | undefined*
{: .signature }

`array` TList\<T\> | undefined
: Specifies the array, collection or enumerable object.

`default` Optional T
: Optional parameter which specifies the default value if there is no first item.

#### Returns
Returns the last item.
