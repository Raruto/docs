---
source: sections/_api_functions/scheduleNumberOfFrames.md
title: scheduleNumberOfFrames
bookmark: schedule-number-of-frames
description: Calls the supplied function at a specific number of frames.
endpoint: function
signature: true
---

#### Signature
##### **scheduleNumberOfFrames**(animation: *function | undefined*, frames: *number*, ...arguments: *any[]*): *void*
{: .signature }

`animation` function | undefined
: Specifies the function to execute.

`frames` number
: Specifies the number of frames.

`...arguments` any[]
: Optional additional arguments which will be passed to the callee.

#### Returns
Nothing
