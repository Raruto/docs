---
source: sections/_api_functions/scheduleUITimeout.md
title: scheduleUITimeout
bookmark: schedule-ui-timeout
description: Schedules the supplied function and invokes it on the specified timeout (using `requestAnimationFrame`) which is optimal for UI related timers.
endpoint: function
signature: true
---

#### Signature
##### **scheduleUITimeout**(callee: *function*, timeout: *number*, ...arguments: *any[]*): *number*
{: .signature }

`callee` function
: Specifies the function to execute.

`timeout` number
: Specifies the timeout in milliseconds.

`...arguments` any[]
: Optional additional arguments which will be passed to the callee.

#### Returns
Returns the call handle.
