---
source: sections/_api_functions/callFunction.md
title: callFunction
bookmark: call-function
description: Calls the supplied function.
endpoint: function
signature: true
---

#### Signature
##### **callFunction**\<T\>(callee: *any*, ...arguments: *any[]*): *any*
{: .signature }

`callee` any
: Specifies the function to execute.

`...arguments` any[]
: Optional additional arguments which will be passed to the callee.

#### Returns
Returns `undefined` if the call fails or the return value of the function which is executed if the call succeeded.
