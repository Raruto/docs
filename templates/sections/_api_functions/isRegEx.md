---
source: sections/_api_functions/isRegEx.md
title: isRegEx
bookmark: is-reg-ex
description: Validates if the supplied variable is a regular expression.
endpoint: function
signature: true
code: |
  ``` typescript
  isRegEx(/[a-z]+/); // Returns `true`
  ```
---

#### Signature
##### **isRegEx**(regEx: *any*): *boolean*
{: .signature }

`regEx` any
: Variable to validate.

#### Returns
Returns `true` if the variable is a regular expression.
