---
source: sections/_api_functions/isCollection.md
title: isCollection
bookmark: is-collection
description: Validates if the supplied variable is a collection. A collection is a list which is enumerable using a `length` property and an `item` function.
endpoint: function
signature: true
---

#### Signature
##### **isCollection**(collection: *any*): *boolean*
{: .signature }

`collection` any
: Variable to validate.

#### Returns
Returns `true` if the variable is a collection.
