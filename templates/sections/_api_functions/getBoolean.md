---
source: sections/_api_functions/getBoolean.md
title: getBoolean
bookmark: get-boolean
description: Retrieves the value of the specified variable in the object array.
endpoint: function
signature: true
---

#### Signature
##### **getBoolean**\<T\>(object: *IObject | undefined*, name: *string*, default?: *boolean*): *boolean*
{: .signature }

`object` IObject | undefined
: Reference to the object.

`name` string
: Name of the variable.

`default` Optional boolean
: Specifies the default value (defaults to `false` if omitted).

#### Returns
Returns the value of the variable.
