---
source: sections/_api_functions/isDefined.md
title: isDefined
bookmark: is-defined
description: Validates if the supplied variable is defined.
endpoint: function
signature: true
code: |
  ``` typescript
  isDefined(1); // Returns `true`
  isDefined(); // Returns `false`
  ```
---

#### Signature
##### **isDefined**(variable: *any*): *boolean*
{: .signature }

`variable` any
: Variable to validate.

#### Returns
Returns `true` if the variable is defined.
