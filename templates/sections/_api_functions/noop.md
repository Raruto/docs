---
source: sections/_api_functions/noop.md
title: noop
bookmark: noop
description: The mother of all helper functions. Does nothing.
endpoint: function
signature: true
---

#### Signature
##### **noop**(): *void*
{: .signature }

#### Returns
Nothing
