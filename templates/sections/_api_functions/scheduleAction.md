---
source: sections/_api_functions/scheduleAction.md
title: scheduleAction
bookmark: schedule-action
description: Calls the supplied function as part of an action in a waiting loop (after 5 animation frames).
endpoint: function
signature: true
  ```
---

#### Signature
##### **scheduleAction**(action: *function | undefined*, ...arguments: *any[]*): *void*
{: .signature }

`action` function | undefined
: Specifies the function to execute.

`...arguments` any[]
: Optional additional arguments which will be passed to the callee.

#### Returns
Nothing
