---
source: sections/_api_functions/callEx.md
title: callEx
bookmark: call-ex
description: Calls the supplied function synchronous or asynchronous.
endpoint: function
signature: true
---

#### Signature
##### **callEx**(callee: *function | undefined*, synchronous: *boolean*, ...arguments: *any[]*): *void*
{: .signature }

`callee` function | undefined
: Specifies the function to execute.

`synchronous` boolean
: Specifies if the function should be executed synchronously.

`...arguments` any[]
: Optional additional arguments which will be passed to the callee.

#### Returns
Returns the call handle for asynchronous calls or `0` for synchronous calls.
