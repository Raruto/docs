---
source: sections/_api_functions/castToFloat.md
title: castToFloat
bookmark: cast-to-float
description: Cast a variable to a floating point number.
endpoint: function
signature: true
code: |
  ``` typescript
  castToFloat("1"); // Returns `1`
  castToFloat("1.5"); // Returns `1.5`
  castToFloat(undefined); // Returns `0`
  ```
---

#### Signature
##### **castToFloat**(value: *any*, default?: *number*): *number*
{: .signature }

`value` any
: Source variable.

`default` Optional number
: Optional parameter which specifies the default floating point number if the supplied source variable cannot be casted.

#### Returns
Returns the floating point number value.
