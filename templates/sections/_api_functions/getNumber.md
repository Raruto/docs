---
source: sections/_api_functions/getNumber.md
title: getNumber
bookmark: get-number
description: Retrieves a numeric value of the specified variable in the object array.
endpoint: function
signature: true
---

#### Signature
##### **getNumber**\<T\>(object: *IObject | undefined*, name: *string*, default?: *number*): *number*
{: .signature }

`object` IObject | undefined
: Reference to the object.

`name` string
: Name of the variable.

`default` Optional number
: Specifies the default value (defaults to `0` if omitted).

#### Returns
Returns the value of the variable.
