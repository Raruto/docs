---
source: sections/_api_builder_forms_radiobutton/focus.md
title: focus
bookmark: focus
description: Sets the focus.
endpoint: method
signature: true
---

#### Signature
##### **focus**(to?: *"first" | "last"*): *boolean*
{: .signature }

`features` Optional "first" | "last"
: Set focus to the first or last option (defaults to `first` if omitted).

#### Returns
Returns `true` if the focus is set.
