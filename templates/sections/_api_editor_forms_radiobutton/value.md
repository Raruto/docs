---
source: sections/_api_builder_forms_radiobutton/value.md
title: value
bookmark: value
description: Retrieves or specifies the selected value.
endpoint: property
signature: true
---

#### Signature
##### **value**: *T | undefined*
{: .signature }
