---
source: sections/_api_builder_forms_radiobutton/select.md
title: select
bookmark: select
description: Selects the specified option.
endpoint: method
signature: true
---

#### Signature
##### **select**(value: *T*): *this*
{: .signature }

`value` T
: Specifies the option value.

#### Returns
Returns a reference to the control to allow chaining.
