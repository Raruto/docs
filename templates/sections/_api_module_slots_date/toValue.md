---
source: sections/_api_module_slots_boolean/toValue.md
title: toValue
bookmark: to-value
description: Converts the supplied data to a date value.
endpoint: method
signature: true
---

#### Signature
##### **toValue**(data: *TSerializeTypes | Date*): *number*
{: .signature }

`data` TSerializeTypes | Date
: Specifies the data.

#### Returns
Returns the date value.
