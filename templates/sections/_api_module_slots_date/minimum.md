---
source: sections/_api_module_slots_date/minimum.md
title: minimum
bookmark: minimum
description: Contains the minimum date.
endpoint: property
signature: true
---

#### Signature
##### **minimum**?: *Date | number*
{: .signature }
