---
source: sections/_api_module_slots_boolean/toDate.md
title: toDate
bookmark: to-date
description: Converts the supplied data to a valid date object.
endpoint: method
signature: true
---

#### Signature
##### **toDate**(data: *TSerializeTypes | Date*): *Date*
{: .signature }

`data` TSerializeTypes | Date
: Specifies the data.

#### Returns
Returns the data as a date object.
