---
source: sections/_api_builder_forms_text/escape.md
title: escape
bookmark: escape
description: Specifies the function which is invoked when the user presses the escape key.
endpoint: method
signature: true
---

#### Signature
##### **escape**\<T\>(escape: *function*): *this*
{: .signature }

`escape` function
: Specifies the function to invoke. To cancel the normal behavior return `true` within the function. The `T` instance is supplied as argument to the function.

#### Returns
Returns a reference to the control to allow chaining.
