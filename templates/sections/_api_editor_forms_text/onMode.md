---
source: sections/_api_builder_forms_text/onMode.md
title: onMode
bookmark: on-mode
description: Invoked when the control mode is changed.
endpoint: method
signature: true
---

#### Signature
##### **onMode**(): *void*
{: .signature }

#### Returns
`void`
