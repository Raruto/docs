---
source: sections/_api_builder_forms_text/focus.md
title: focus
bookmark: focus
description: Sets the focus to the control.
endpoint: method
signature: true
---

#### Signature
##### **focus**(): *boolean*
{: .signature }

#### Returns
Returns `true` if the focus is set.
