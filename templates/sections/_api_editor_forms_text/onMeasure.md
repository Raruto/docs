---
source: sections/_api_builder_forms_text/onMeasure.md
title: onMeasure
bookmark: on-measure
description: Invoked when the text control is first measured.
endpoint: method
signature: true
---

#### Signature
##### **onMeasure**(): *number*
{: .signature }

#### Returns
Returns the measure correction for the text control.
