---
source: sections/_api_builder_forms_text/sanitize.md
title: sanitize
bookmark: sanitize
description: Specifies if string sanitizing should be applied.
endpoint: method
signature: true
---

#### Signature
##### **sanitize**(sanitize?: *boolean*): *this*
{: .signature }

`sanitize` Optional boolean
: Specifies if string sanitizing should be applied (defaults to `true` if omitted).

#### Returns
Returns a reference to the control to allow chaining.
