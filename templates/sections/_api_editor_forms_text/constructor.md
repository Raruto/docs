---
source: sections/_api_builder_forms_text/constructor.md
title: constructor
bookmark: constructor
description: Creates a new text instance.
endpoint: constructor
signature: true
---

#### Signature
##### new Forms.**Text**(type?: *Types*, value?: *TBinding\<string\>*, lines?: *number*, style?: *ITextStyle*): *Text*
{: .signature }

`type` Optional Types
: Specifies the input type (defaults to `singleline` if omitted). Types:
- `singleline`
- `multiline`
- `multiline-wo-crlf`
- `password`
- `email`

`value` Optional TBinding\<string\>
: Specifies the initial value (defaults to `""` if omitted).

`lines` number
: Specifies the minimum number of lines for multi-line input (defaults to `2` if omitted).

`style` Optional ITextStyle
: Specifies the optional style.

#### Returns
`Text`
