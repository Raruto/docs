---
source: sections/_api_builder_forms_text/placeholder.md
title: placeholder
bookmark: placeholder
description: Sets a placeholder for the text control.
endpoint: method
signature: true
---

#### Signature
##### **placeholder**(placeholder: *string*): *this*
{: .signature }

`placeholder` string
: Specifies the placeholder text.

#### Returns
Returns a reference to the control to allow chaining.
