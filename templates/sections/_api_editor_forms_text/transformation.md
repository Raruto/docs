---
source: sections/_api_builder_forms_text/transformation.md
title: transformation
bookmark: transformation
description: Specifies the text transformation.
endpoint: method
signature: true
---

#### Signature
##### **transformation**(transformation: *Transformations*): *this*
{: .signature }

`transformation` Transformations
: Specifies the transformation type. Transformations:
- `none`
- `capitalize`
- `capitalize-words`
- `capitalize-sentences`
- `uppercase`
- `lowercase`

#### Returns
Returns a reference to the control to allow chaining.
