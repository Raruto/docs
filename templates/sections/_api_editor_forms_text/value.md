---
source: sections/_api_builder_forms_text/value.md
title: value
bookmark: value
description: Retrieves or specifies the input value.
endpoint: property
signature: true
---

#### Signature
##### **value**: *string*
{: .signature }
