---
source: sections/_api_builder_forms_text/autoValidate.md
title: autoValidate
bookmark: auto-validate
description: Enables automatic control validation.
endpoint: method
signature: true
---

#### Signature
##### **autoValidate**(validation?: *TControlValidation\<Text\>*): *this*
{: .signature }

`validation` Optional TControlValidation\<Text\>
: Specifies the validator.

#### Returns
Returns a reference to the control to allow chaining.
