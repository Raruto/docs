---
source: sections/_api_builder_forms_text/type.md
title: type
bookmark: type
description: Retrieves the input type.
endpoint: property
readonly: true
signature: true
---

#### Signature
##### **type**: *Types*
{: .signature }
