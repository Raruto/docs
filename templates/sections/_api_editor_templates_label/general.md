---
source: sections/_api_builder_templates_label/general.md
title: general
bookmark: general
description: Lorem ipsum.
endpoint: method
signature: true
---

#### Signature
##### **general**(controller: *BuilderController*): *Feature*
{: .signature }

`controller` BuilderController
: Reference to the controller.

#### Returns
Returns a reference to the feature.
