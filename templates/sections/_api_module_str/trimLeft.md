---
source: sections/_api_module_str/trimLeft.md
title: trimLeft
bookmark: trim-left
description: Trims a string at the left side by removing all leading whitespaces (`_ab__cd_` -> `ab__cd_`).
endpoint: function
signature: true
code: |
  ``` typescript
  trimLeft(" ab  cd "); // Returns `ab  cd `
  ```
---

#### Signature
##### Str.**trimLeft**(value: *string*): *string*
{: .signature }

`value` string
: Source string.

#### Returns
Returns the trimmed string.
