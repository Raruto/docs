---
source: sections/_api_module_str/limit.md
title: limit
bookmark: limit
description: Limits a string which is greater than the specified number of characters and appends an optional string. If an append string is specified any trailing spaces in the trimmed string are removed.
endpoint: function
signature: true
code: |
  ``` typescript
  limit("Lorem ipsum", 5, "..."); // Returns `Lorem...`
  limit("Lorem ipsum", 6, "..."); // Returns `Lorem...`
  limit("Lorem ipsum", 6); // Returns `Lorem `
  ```
---

#### Signature
##### Str.**limit**(value: *string*, max: *number*, append?: *string*): *string*
{: .signature }

`value` string
: Specifies the input string (variable will be casted to a string if necessary).

`max` number
: Maximum string length.

`append` Optional string
: Optional string which is appended to a limited string (defaults to `""` if omitted).

#### Returns
Returns the limited string.
