---
source: sections/_api_module_str/fill.md
title: fill
bookmark: fill
description: Fills a string for the specified times with the specified string or characters.
endpoint: function
signature: true
code: |
  ``` typescript
  fill("A", 5); // Returns `AAAAA`
  ```
---

#### Signature
##### Str.**fill**(fill: *string*, count: *number*): *string*
{: .signature }

`fill` string
: Specifies the fill string.

`count` number
: Specifies the number of copies to insert.

#### Returns
Returns the filled string.
