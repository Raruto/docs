---
source: sections/_api_module_str/replace.md
title: replace
bookmark: replace
description: Replaces all occurrences of `what` with `with` in the specified string.
endpoint: function
signature: true
code: |
  ``` typescript
  replace("Hello", "l", "-"); // Returns `He--o`
  ```
---

#### Signature
##### Str.**replace**(value: *string*, what: *string*, with?: *string*, ignoreCase?: *boolean*): *string*
{: .signature }

`value` string
: Specifies the input string (variable will be casted to a string if necessary).

`what` string
: Specifies the string to search for.

`with` Optional string
: Specifies the replace string. If omitted an empty string will be used.

`ignoreCase` Optional boolean
: Specifies if the string replace should be case insensitive (defaults to `false` if omitted).

#### Returns
Returns the replaced string.
