---
source: sections/_api_module_str/lowercase.md
title: lowercase
bookmark: lowercase
description: Converts a string to lowercase.
endpoint: function
signature: true
code: |
  ``` typescript
  lowercase("HELLO"); // Returns `hello`
  ```
---

#### Signature
##### Str.**lowercase**(value: *string*): *string*
{: .signature }

`value` string
: Source string.

#### Returns
Returns the converted string.
