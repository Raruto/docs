---
source: sections/_api_module_str/trim.md
title: trim
bookmark: trim
description: Trims a string by removing all leading and trailing whitespaces (`_ab__cd_` -> `ab__cd`).
endpoint: function
signature: true
code: |
  ``` typescript
  trim(" ab  cd "); // Returns `ab  cd`
  ```
---

#### Signature
##### Str.**trim**(value: *string*): *string*
{: .signature }

`value` string
: Source string.

#### Returns
Returns the trimmed string.
