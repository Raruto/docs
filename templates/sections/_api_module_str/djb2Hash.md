---
source: sections/_api_module_str/djb2Hash.md
title: djb2Hash
bookmark: djb2-hash
description: Creates a simple hash for the supplied string using the djb2-algorithm written by Dan Bernstein. This hash function is similar to a linear congruential generator and is absolutely not collision resistant.
endpoint: function
signature: true
code: |
  ``` typescript
  djb2Hash("Abc"); // Returns `ABCAA`
  djb2Hash("Abc", "prefix-"); // Returns `prefix-ABCAA`
  ```
---

#### Signature
##### Str.**djb2Hash**(value: *string*, prefix?: *string*): *string*
{: .signature }

`value` string
: Specifies the input string.

`prefix` Optional string
: Specifies a prefix for the hash result (defaults to `""` if omitted).

#### Returns
Returns the hash string.
