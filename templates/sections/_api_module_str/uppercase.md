---
source: sections/_api_module_str/uppercase.md
title: uppercase
bookmark: uppercase
description: Converts a string to uppercase.
endpoint: function
signature: true
code: |
  ``` typescript
  uppercase("Hello"); // Returns `HELLO`
  ```
---

#### Signature
##### Str.**uppercase**(value: *string*): *string*
{: .signature }

`value` string
: Source string.

#### Returns
Returns the converted string.
