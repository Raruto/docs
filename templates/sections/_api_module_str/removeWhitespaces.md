---
source: sections/_api_module_str/removeWhitespaces.md
title: removeWhitespaces
bookmark: remove-whitespaces
description: Removes all white spaces from the specified string (`_ab__cd_` -> `abcd`).
endpoint: function
signature: true
code: |
  ``` typescript
  removeWhitespaces(" ab  cd "); // Returns `abcd`
  ```
---

#### Signature
##### Str.**removeWhitespaces**(value: *string*): *string*
{: .signature }

`value` string
: Source string.

#### Returns
Returns the sanitized string.
