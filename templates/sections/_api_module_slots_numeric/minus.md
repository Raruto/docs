---
source: sections/_api_module_slots_numeric/minus.md
title: minus
bookmark: minus
description: Contains the minus sign.
endpoint: property
signature: true
---

#### Signature
##### **minus**?: *string*
