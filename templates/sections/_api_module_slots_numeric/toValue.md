---
source: sections/_api_module_slots_numeric/toValue.md
title: toValue
bookmark: to-value
description: Converts the supplied data to a valid numeric value.
endpoint: method
signature: true
---

#### Signature
##### **toValue**(data: *TSerializeTypes*): *number*
{: .signature }

`data` TSerializeTypes
: Specifies the data.

#### Returns
Returns the numeric value.
