---
source: sections/_api_module_slots_numeric/suffix.md
title: suffix
bookmark: suffix
description: Contains the suffix.
endpoint: property
signature: true
---

#### Signature
##### **suffix**?: *string*
