---
source: sections/_api_module_slots_numeric/separator.md
title: separator
bookmark: separator
description: Contains the thousands separtor sign.
endpoint: property
signature: true
---

#### Signature
##### **separator**?: *string*
