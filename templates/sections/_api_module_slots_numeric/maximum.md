---
source: sections/_api_module_slots_numeric/maximum.md
title: maximum
bookmark: maximum
description: Contains the maximum value.
endpoint: property
signature: true
---

#### Signature
##### **maximum**?: *number*
