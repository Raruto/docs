---
source: sections/_api_module_slots_string/toString.md
title: toString
bookmark: to-string
description: Converts the supplied data to a valid string.
endpoint: method
signature: true
---

#### Signature
##### **toString**(data: *TSerializeTypes*): *string*
{: .signature }

`data` TSerializeTypes
: Specifies the data.

#### Returns
Returns the data as a string.
