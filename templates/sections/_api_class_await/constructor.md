---
source: sections/_api_class_await/constructor.md
title: constructor
bookmark: constructor
description: Creates a new await.
endpoint: constructor
signature: true
---

#### Signature
##### new **Await**(properties: *IAwaitProperties*): *Await*
{: .signature }

`properties` IAwaitProperties
: Specifies the await properties.

#### Returns
A new `Await`.
