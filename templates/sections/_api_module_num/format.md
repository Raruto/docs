---
source: sections/_api_module_num/format.md
title: format
bookmark: format
description: Formats a number by inserting thousands and decimal separators while taking rounding into account.
endpoint: function
signature: true
code: |
  ``` typescript
  format("1000.235", 2); // Returns `1,000.24`
  ```
---

#### Signature
##### Num.**format**(value: *number | string*, precision?: *number*, separator?: *string*, decimal?: *string*, minus?: *string*): *string*
{: .signature }

`value` number | string
: Input value as a number or string.

`precision` Optional number
: Specifies the precision (defaults to `0` if omitted).

`separator` Optional string
: Separator sign (defaults to `,` if omitted).

`decimal` Optional string
: Decimal sign (defaults to `.` if omitted).

`minus` Optional string
: Minus sign (defaults to `-` if omitted).

#### Returns
Returns the formatted number as a string.
