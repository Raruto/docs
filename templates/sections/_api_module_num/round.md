---
source: sections/_api_module_num/round.md
title: round
bookmark: round
description: Round a floating point number to the nearest integer.
endpoint: function
signature: true
code: |
  ``` typescript
  round(1.49); // Returns `1`
  round(1.5); // Returns `2`
  ```
---

#### Signature
##### Num.**round**(value: *number*): *number*
{: .signature }

`value` number
: Input number.

#### Returns
Returns the rounded number.
