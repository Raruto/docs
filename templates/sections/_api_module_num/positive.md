---
source: sections/_api_module_num/positive.md
title: positive
bookmark: positive
description: Checks if the supplied number is positive and returns this positive value or `0` if the value is negative.
endpoint: function
signature: true
code: |
  ``` typescript
  positive(1); // Returns `1`
  positive(-1); // Returns `0`
  ```
---

#### Signature
##### Num.**positive**(value: *number*): *number*
{: .signature }

`value` number
: Input number.

#### Returns
Returns `value` if the value is positive otherwise `0`.
