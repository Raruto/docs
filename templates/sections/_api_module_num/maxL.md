---
source: sections/_api_module_num/maxL.md
title: maxL
bookmark: max-l
description: Compares the supplied arguments returns the highest value.
endpoint: function
signature: true
code: |
  ``` typescript
  maxL(1, 2, 5, 3); // Returns `5`
  ```
---

#### Signature
##### Num.**maxL**(...pArguments: *number[]*): *number*
{: .signature }

`...pArguments` number[]
: Arguments to compare.

#### Returns
Returns the number with the highest value.
