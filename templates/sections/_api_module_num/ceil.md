---
source: sections/_api_module_num/ceil.md
title: ceil
bookmark: ceil
description: Round a floating point number upward to its nearest integer.
endpoint: function
signature: true
code: |
  ``` typescript
  ceil(1.6); // Returns `2`
  ```
---

#### Signature
##### Num.**ceil**(value: *number*): *number*
{: .signature }

`value` number
: Input number.

#### Returns
Returns the ceiled number.
