---
source: sections/_api_module_num/negative.md
title: negative
bookmark: negative
description: Checks if the supplied number is negative and returns this negative value or `0` if the value is positive.
endpoint: function
signature: true
code: |
  ``` typescript
  negative(1); // Returns `0`
  negative(-1); // Returns `-1`
  ```
---

#### Signature
##### Num.**negative**(value: *number*): *number*
{: .signature }

`value` number
: Input number.

#### Returns
Returns `value` if the value is negative otherwise `0`.
