---
source: sections/_api_builder_templates/visibility.md
title: visibility
bookmark: visibility
description: Adds the disabled feature which controls the state of a node.
endpoint: method
signature: true
---

#### Signature
##### **visibility**(block: *NodeBlock | Node*, controller: *BuilderController*): *Feature*
{: .signature }

`block` NodeBlock | Node
: Reference to the block or node.

`controller` BuilderController
: Reference to the controller.

#### Returns
Returns a reference to the feature.
