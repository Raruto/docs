---
source: sections/_api_builder_templates/alias.md
title: alias
bookmark: alias
description: Adds the alias feature.
endpoint: method
signature: true
---

#### Signature
##### **alias**(slot: *Slot | undefined*, controller: *BuilderController*): *Feature | undefined*
{: .signature }

`slot` Slot | undefined
: Reference to a slot.

`controller` BuilderController
: Reference to the controller.

#### Returns
Returns a reference to the feature.
