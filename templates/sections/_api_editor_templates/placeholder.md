---
source: sections/_api_builder_templates/placeholder.md
title: placeholder
bookmark: placeholder
description: Adds the placeholder feature.
endpoint: method
signature: true
---

#### Signature
##### **placeholder**(block: *NodeBlock | Node | Slot*, controller: *BuilderController*, name: *Feature*): *Feature*
{: .signature }

`block` NodeBlock | Node
: Reference to the block or node.

`controller` BuilderController
: Reference to the controller.

`name` Feature
: Reference to the name feature.

#### Returns
Returns a reference to the feature.
