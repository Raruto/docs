---
source: sections/_api_module_slots_slots/clean.md
title: clean
bookmark: clean
description: Removes all the slots from the collection except the supplied exclude slots.
endpoint: method
signature: true
---

#### Signature
##### **clean**(kind: *Kinds | "*"*, ...exlcude: *(undefined | Slot\<undefined | null | string | number | false | true | ISerialize | (undefined | null | string | number | false | true | ISerialize)[]\>)[]*): *void*
{: .signature }

`kind` Kinds | "*"
: Specifies the kind of slots to remove.

`...exlcude` (undefined | Slot\<undefined | null | string | number | false | true | ISerialize | (undefined | null | string | number | false | true | ISerialize)[]\>)[]
: Specifies the slots to exclude. Those are kept in the slots collection.

#### Returns
Nothing
