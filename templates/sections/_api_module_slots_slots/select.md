---
source: sections/_api_module_slots_slots/select.md
title: select
bookmark: select
description: Selects a slot.
endpoint: method
signature: true
---

#### Signature
##### **select**\<T\>(referenceOrHash: *string*, kind?: *Kinds*): *T | undefined*
{: .signature }

`referenceOrHash` string
: Reference or hash of the slot.

`kind` Optional Kinds
: Specifies the optional kind of the slot.

#### Returns
Returns the slot or `undefined` if no slot is found.
