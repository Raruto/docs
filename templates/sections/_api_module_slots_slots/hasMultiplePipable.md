---
source: sections/_api_module_slots_slots/hasMultiplePipable.md
title: hasMultiplePipable
bookmark: has-multiple-pipable
description: Retrieves if multiple pipable slots are present.
endpoint: property
readonly: true
signature: true
---

#### Signature
##### **hasMultiplePipable**(): *boolean*
{: .signature }
