---
source: sections/_api_module_slots_slots/destroy.md
title: destroy
bookmark: destroy
description: Destroys the slots instance.
endpoint: method
signature: true
---

#### Signature
##### **destroy**(): *void*
{: .signature }

#### Returns
Nothing
