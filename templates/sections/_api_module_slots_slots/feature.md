---
source: sections/_api_module_slots_slots/feature.md
title: feature
bookmark: feature
description: Creates a new feature slot.
endpoint: method
signature: true
---

#### Signature
##### **feature**\<T\>(type: *ISlotType\<T\>*, reference: *string*, sequence?: *number*): *T*
{: .signature }

`type` ISlotType\<T\>
: Specifies the slot type.

`reference` string
: Specifies the reference.

`sequence` Optional number
: Specifies the sequence number (defaults to `0` if omitted).

#### Returns
Returns a reference to the slot.
