---
source: sections/_api_module_slots_slots/deserialize.md
title: deserialize
bookmark: deserialize
description: Deserializes slots.
endpoint: method
signature: true
---

#### Signature
##### **deserialize**(slots: *ISlot[]*): *this*
{: .signature }

`slots` ISlot[]
: Specifies the slots array.

#### Returns
Returns a reference to the instance.
