---
source: sections/_api_module_slots_slots/serialize.md
title: serialize
bookmark: serialize
description: Serializes the slots to an array.
endpoint: method
signature: true
---

#### Signature
##### **serialize**(): *ISlot[]*
{: .signature }

#### Returns
Returns the slots array.
