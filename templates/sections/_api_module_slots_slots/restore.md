---
source: sections/_api_module_slots_slots/restore.md
title: restore
bookmark: restore
description: Restores the supplied slot.
endpoint: method
signature: true
---

#### Signature
##### **restore**\<T\>(slot: *T*): *T*
{: .signature }

`slot` T
: Reference to the slot.

#### Returns
Returns a reference to the slot.
