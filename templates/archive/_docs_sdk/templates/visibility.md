
---
---

#### Signature
##### **visibility**(block: *NodeBlock | Node*, controller: *BuilderController*): *Feature*
{: .signature }

#### Description
Adds the disabled feature which controls the state of a node.

`block` NodeBlock | Node
: Reference to the block or node.

`controller` BuilderController
: Reference to the controller.

#### Returns
Returns a reference to the feature.
