
---
---

#### Signature
##### **placeholder**(block: *NodeBlock | Node | Slot*, controller: *BuilderController*, name: *Feature*): *Feature*
{: .signature }

#### Description
Adds the placeholder feature.

`block` NodeBlock | Node
: Reference to the block or node.

`controller` BuilderController
: Reference to the controller.

`name` Feature
: Reference to the name feature.

#### Returns
Returns a reference to the feature.
