
---
---

#### Signature
##### **general**(controller: *BuilderController*): *Feature*
{: .signature }

#### Description
`controller` BuilderController
: Reference to the controller.

#### Returns
Returns a reference to the feature.
