
---
---

#### Signature
##### **name**(block: *NodeBlock | Node*, controller: *BuilderController*, toggle: *boolean*, title: *string*): *Feature*
{: .signature }

#### Description
Adds the name feature.

`block` NodeBlock | Node
: Reference to the block or node.

`controller` BuilderController
: Reference to the controller.

`toggle` boolean
: Specifies if the name visibility can be toggled.

`title` string
: Specifies the title for the feature.

#### Returns
Returns a reference to the feature.
