
---
---

#### Signature
##### **required**(slot: *Slot | undefined*, controller: *BuilderController*, label: *string*): *Feature | undefined*
{: .signature }

#### Description
Adds the data required feature.

`slot` Slot | undefined
: Reference to a slot.

`controller` BuilderController
: Reference to the controller.

`label` string
: Specifies the label for the feature.

#### Returns
Returns a reference to the feature.
