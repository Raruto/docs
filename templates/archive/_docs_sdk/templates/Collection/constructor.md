---
---

#### Signature
##### new **Collection**(configuration: *ICollectionConfiguration\<T, Reference\>*): *Collection*
{: .signature }

#### Description
Creates a new collection card.

`configuration` ICollectionConfiguration\<T, Reference\>
: Specifies the configuration for the collection.

#### Returns
`Collection`
