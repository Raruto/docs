---
---

#### Signature
##### **deleteUnnamed**(): *void*
{: .signature }

#### Description
Deletes all unnamed items from the collection.
