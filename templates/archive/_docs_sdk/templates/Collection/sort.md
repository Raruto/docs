---
---

#### Signature
##### **sort**(direction: *"ascending" | "descending"*): *void*
{: .signature }

#### Description
Sorts the list with items.

`direction` "ascending" | "descending"
: Specifies the sort direction.
