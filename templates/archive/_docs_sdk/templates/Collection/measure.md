---
---

#### Signature
##### **measure**(): *boolean[]*
{: .signature }

#### Description
Measures the size of the card.

#### Returns
Returns `true` if the size is changed.
