
---
---

#### Signature
##### **placeholder**(): *string* (Readonly)
{: .signature }

#### Description
Retrieves the item placeholder.
