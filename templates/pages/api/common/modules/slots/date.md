---
base: ../../../../../
permalink: /api/common/modules/slots/date
title: Date / Slots / Modules / Common - API - Tripetto Documentation
custom: true
---

{% assign sections = site.api_module_slots_date | sort: "path" %}

<section>
    <section>
        <h2>
            Date
            <span class="endpoint class"></span>
            <a href="" class="bookmark"></a>
        </h2>
        <p class="description">Lorem ipsum.</p>
        {% include toc.html %}
    </section>
</section>

{% include sections.html %}
