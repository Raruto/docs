---
base: ../../../../../
permalink: /api/common/modules/slots/number
title: Number / Slots / Modules / Common - API - Tripetto Documentation
custom: true
---

{% assign sections = site.api_module_slots_number | sort: "path" %}

<section>
    <section>
        <h2>
            Number
            <span class="endpoint class"></span>
            <a href="" class="bookmark"></a>
        </h2>
        <p class="description">Lorem ipsum.</p>
        {% include toc.html %}
    </section>
</section>

{% include sections.html %}
