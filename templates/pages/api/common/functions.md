---
base: ../../../
permalink: /api/common/functions/
title: Pure helper functions / Common - API - Tripetto Documentation
custom: true
---

{% assign sections = site.api_functions | sort: "path" %}

<section>
    <section>
        <h2>
            Pure helper functions
            <a href="" class="bookmark"></a>
        </h2>
        <p class="description">Functional toolkit for a variety of tasks.</p>
        {% include toc.html %}
    </section>
</section>

{% include sections.html %}
