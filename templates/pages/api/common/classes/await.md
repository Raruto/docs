---
base: ../../../../
permalink: /api/common/classes/await/
title: Await / Classes / Common - API - Tripetto Documentation
custom: true
---

{% assign sections = site.api_class_await | sort: "path" %}

<section>
    <section>
        <h2>
            Await
            <span class="endpoint class"></span>
            <a href="" class="bookmark"></a>
        </h2>
        <p class="description">Implements a simple await-promise mechanism.</p>
        {% include toc.html %}
    </section>
</section>

{% include sections.html %}
