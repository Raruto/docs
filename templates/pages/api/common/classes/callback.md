---
base: ../../../../
permalink: /api/common/classes/callback/
title: Callback / Classes / Common - API - Tripetto Documentation
custom: true
---

{% assign sections = site.api_class_callback | sort: "path" %}

<section>
    <section>
        <h2>
            Callback
            <span class="endpoint class"></span>
            <a href="" class="bookmark"></a>
        </h2>
        <p class="description">Implements an await-promise mechanism which supports callback return values.</p>
        {% include toc.html %}
    </section>
</section>

{% include sections.html %}
