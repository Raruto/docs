---
base: ../../../
permalink: /api/builder/builder/
title: Builder / Builder - API - Tripetto Documentation
custom: true
---

{% assign sections = site.api_builder_builder | sort: "path" %}

<section>
    <section>
        <h2>
            Builder
            <a href="" class="bookmark"></a>
        </h2>
        <p class="description">Lorem ipsum.</p>
        {% include toc.html %}
    </section>
</section>

{% include sections.html %}
