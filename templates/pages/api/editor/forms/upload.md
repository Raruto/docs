---
base: ../../../../
permalink: /api/builder/forms/upload/
title: Upload / Forms / Builder - API - Tripetto Documentation
custom: true
---

{% assign sections = site.api_builder_forms_upload | sort: "path" %}

<section>
    <section>
        <h2>
            Upload
            <span class="endpoint class"></span>
            <a href="" class="bookmark"></a>
        </h2>
        <p class="description">This class implements the file upload control.</p>
        {% include toc.html %}
    </section>
</section>

{% include sections.html %}
