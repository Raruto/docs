---
base: ../../../../
permalink: /api/builder/forms/button/
title: Button / Forms / Builder - API - Tripetto Documentation
custom: true
---

{% assign sections = site.api_builder_forms_button | sort: "path" %}

<section>
    <section>
        <h2>
            Button
            <span class="endpoint class"></span>
            <a href="" class="bookmark"></a>
        </h2>
        <p class="description">This class implements a form button.</p>
        {% include toc.html %}
    </section>
</section>

{% include sections.html %}
