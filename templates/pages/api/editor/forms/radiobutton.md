---
base: ../../../../
permalink: /api/builder/forms/radiobutton/
title: Radiobutton / Forms / Builder - API - Tripetto Documentation
custom: true
---

{% assign sections = site.api_builder_forms_radiobutton | sort: "path" %}

<section>
    <section>
        <h2>
            Radiobutton
            <span class="endpoint class"></span>
            <a href="" class="bookmark"></a>
        </h2>
        <p class="description">This class implements the form radiobutton control.</p>
        {% include toc.html %}
    </section>
</section>

{% include sections.html %}
