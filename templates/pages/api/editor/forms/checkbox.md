---
base: ../../../../
permalink: /api/builder/forms/checkbox/
title: Checkbox / Forms / Builder - API - Tripetto Documentation
custom: true
---

{% assign sections = site.api_builder_forms_checkbox | sort: "path" %}

<section>
    <section>
        <h2>
            Checkbox
            <span class="endpoint class"></span>
            <a href="" class="bookmark"></a>
        </h2>
        <p class="description">This class implements a checkbox control.</p>
        {% include toc.html %}
    </section>
</section>

{% include sections.html %}
