---
base: ../../../../
permalink: /api/builder/forms/numeric/
title: Numeric / Forms / Builder - API - Tripetto Documentation
custom: true
---

{% assign sections = site.api_builder_forms_numeric | sort: "path" %}

<section>
    <section>
        <h2>
            Numeric
            <span class="endpoint class"></span>
            <a href="" class="bookmark"></a>
        </h2>
        <p class="description">This class implements the form numeric control. The control supports fixed and floating point numbers. Automatic rounding is applied based on the desired numeric precision.</p>
        {% include toc.html %}
    </section>
</section>

{% include sections.html %}
