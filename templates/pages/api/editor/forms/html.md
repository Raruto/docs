---
base: ../../../../
permalink: /api/builder/forms/html/
title: HTML / Forms / Builder - API - Tripetto Documentation
custom: true
---

{% assign sections = site.api_builder_forms_html | sort: "path" %}

<section>
    <section>
        <h2>
            HTML
            <span class="endpoint class"></span>
            <a href="" class="bookmark"></a>
        </h2>
        <p class="description">This class implements a form HTML control which can be used to insert custom HTML.</p>
        {% include toc.html %}
    </section>
</section>

{% include sections.html %}
