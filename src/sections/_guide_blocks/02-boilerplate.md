---
source: sections/_guide_blocks/02-boilerplate.md
bookmark: boilerplate
title: Boilerplate
---

We've created a [boilerplate](https://gitlab.com/{{ site.accounts.gitlab }}/blocks/boilerplate){:target="_blank"} to help you start building blocks. It contains the recommended packages, boilerplate code and tasks to get things up and running smoothly. We suggest using the boilerplate as a starting point when you are going to develop a block. To do so, start by downloading and extracting the boilerplate from the following URL to any local folder you like:

```
https://gitlab.com/{{ site.accounts.gitlab }}/blocks/boilerplate/repository/master/archive.zip
```

Next, open your terminal/command prompt and go to your newly created folder. Execute the following command (make sure you have [npm](https://www.npmjs.com/){:target="_blank"} and [Node.js](https://nodejs.org/){:target="_blank"} installed) to install all the required packages:

```bash
$ npm install
```

To start developing and testing your block run the following command:

```bash
$ npm start
```

This will start the builder with a (initial empty) form (stored in `./test/example.json`). The builder will restart automatically with every code change.

### Start with a clean installation
If you want to start from scratch or develop blocks in an existing project, you can install the builder package as a dependency using the following command:
```bash
$ npm install {{ site.npm_packages.builder }} --save-dev
```

It contains the builder application as well as the TypeScript declaration files (typings) necessary for block development. The typings should be discovered automatically by your IDE when importing symbols from the `{{ site.npm_packages.builder }}` module.

This documentation is updated as we continue our work on Tripetto and build a community. Please [let us know](../support) if you run into issues or need additional information. We’re more than happy to keep you going and also improve the documentation.
{: .warning }
