---
source: sections/_guide_blocks/00-introduction.md
title: Blocks
---

### Taking hardcore charge
Perhaps one of the best things about Tripetto is that you decide which form *building blocks* (e.g. question types) you want to use in the builder and runner. We offer a default set to choose from, but you can also develop your own building blocks.

**In one single package a block typically both:**

- Holds the properties of a particular building block (e.g. [dropdown](https://www.npmjs.com/package/tripetto-block-dropdown){:target="_blank"}, [checkbox](https://www.npmjs.com/package/tripetto-block-checkbox){:target="_blank"}, etc.);
- **And** facilitates the management of those properties through the builder.

For building blocks we recommend using [TypeScript](https://www.typescriptlang.org/){:target="_blank"}. We supply typings to enable optimal [IntelliSense](https://code.visualstudio.com/docs/builder/intellisense){:target="_blank"} support.
{: .info }

This step-by-step guide for building blocks assumes a good understanding of [TypeScript](https://www.typescriptlang.org/){:target="_blank"}, [object-oriented programming](https://www.typescriptlang.org/docs/handbook/classes.html){:target="_blank"} and [webpack](https://webpack.js.org/){:target="_blank"}.
{: .warning }
