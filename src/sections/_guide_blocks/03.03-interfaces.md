---
source: sections/_guide_blocks/03.03-interfaces.md
bookmark: interfaces
name: Interfaces
title: Working with interfaces
sub: true
---

If you have read the [runner](../runner/) documentation, you probably already noticed that Tripetto requires you to implement each block in two domains:

- *Builder domain*: The part of the block that will allow the use of it for smart form building in the builder;
- *Runner domain*: The part of the block that renders it to a visual component inside an actual form in a website or application. If you have different runners with different visual styles, you probably need multiple implementations for each block.

There is no technical overlap between the two implementation domains, except for the block's properties. These properties describe the behavior of your block (they are set with the builder and consumed by the runner) and act as the block contract between the implementation domains.

To avoid duplicate interface declarations or mismatches between the properties, we suggest creating a single interface inside your builder block implementation. This interface then acts as the single point of truth and should be exposed by your builder block package, so you can use it inside each runner implementation. This way the block properties are declared in a single place, making it easier to maintain and more consistent.

![Blocks diagram](../../images/diagrams/blocks.svg)

#### Builder domain
The following example shows how to achieve this in the builder domain. First off we define the interface in a separate type declaration file, for example `interface.d.ts`.

```typescript
export interface IExampleProperties {
  ...
  // Define a property
  color: string;
  ...
}
```

Next, implement a block (in this example `index.ts`).

```typescript
import {
  NodeBlock, tripetto
} from "{{ site.npm_packages.builder }}";
import * as ICON from "./icon.svg";

@tripetto({
    type: "node",
    identifier: "example-block",
    icon: ICON,
    label: "Example block"
  })
export class Example extends NodeBlock {
  ...
  // Make the property part of the form definition
  @definition color = "red";
  ...
}
```

And this is all you need to do in the builder domain. Make sure to reference your type definition inside your `package.json` as follows.

```json
{
  "name": "example-block",
  "version": "1.0.0",
  "main": "index.js",
  "types": "interface.d.ts"
}
```

#### Runner domain
Now you can use the interface that is declared inside the builder block package in your runner implementation. Also make sure to add the block package to your runner. Then you can import the interface type and feed it into your runner block.

```typescript
import { IExampleProperties } from "example-block";

@Tripetto.block({
    type: "node",
    identifier: "example-block"
})
export class ExampleBlock extends NodeBlock<IExampleProperties> {
  ...
  doSomething(): void {
    // Use the property
    console.log(this.props.color); // Outputs `red`
  }
  ...
}
```

This documentation is updated as we continue our work on Tripetto and build a community. Please [let us know](../support) if you run into issues or need additional information. We’re more than happy to keep you going and also improve the documentation.
{: .warning }
