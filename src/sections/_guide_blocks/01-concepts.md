---
source: sections/_guide_blocks/01-concepts.md
bookmark: concepts
title: Concepts
---

This is a good time to highlight again that we built a 2D drawing board because we think that's the best way to visualize and edit an advanced form or survey; especially if it supports logic and conditional flow to smartly adapt to respondents' inputs. This generally shortens forms and enhances their responsiveness. So instead of being a [WYSIWYG](https://en.wikipedia.org/wiki/WYSIWYG){:target="_blank"} builder, it presents the structural layout of a form's flow and lets you easily move around building blocks on a self-organizing grid.

This is where blocks also come in. These **node blocks** and **condition blocks** essentially define building block behaviors in a form and dictate what properties and **feature cards** to unlock in the builder for their configuration. A block instructs the builder. And when instructed correctly by properly formatted block **interfaces**, the builder will know everything it needs to know to handle the pertaining building block on the drawing board and the runner can collect respondent inputs in so-called **slots**.

#### Overview
The following diagram shows the root structure of a form in the builder. This scheme is actually a pattern that can occur recursively. Each cluster can be a repeat of the shown structure, with varying numbers of branches and clusters per branch of course. You'll recognize this recursive pattern quite easily when you start using the [builder](../builder/).

You'll probably notice that the following diagram looks a lot like the diagram we used in the chapter about the runner to explain the core concepts for the structure of Tripetto. That's because we're talking about the exact same concepts here. Yet, we're now taking you a step deeper into how exactly behaviors are coupled with nodes; by injecting **blocks**.
{: .info }

![Builder structure](../../images/diagrams/builder.svg)

#### Entities
Before starting your development of blocks you'll want to familiarize yourself with the following entities:

#### `Nodes`
These are the containers for the actual form building blocks (i.e. element types like text input, dropdown, checkbox etc.). A node is basically a placeholder for a block. The node behavior itself is defined in a *block*.

#### `Clusters`
One or more *nodes* can be placed in a so-called *cluster*. It is simply a collection of nodes.

#### `Branches`
One or more *clusters* can form a *branch*. A branch can be conditional. You can define certain conditions for the branch to be taken or skipped.

#### `Conditions`
A *branch* can contain one or more *conditions*, which are used to direct flow into the branch. They are evaluated when a cluster ends. Only subsequent branches with matching condition(s) are taken by the runner. Just like nodes, the conditions are actually placeholders for condition behaviors. The condition behavior itself is defined in a *condition block*.

#### `Blocks`
So, blocks supply a certain behavior to nodes and conditions that are used in the builder to create smart forms. As mentioned before blocks come in two flavours:
- *Node blocks*: Provide building blocks for nodes (for example [text input](https://www.npmjs.com/package/tripetto-block-text){:target="_blank"}, [dropdown](https://www.npmjs.com/package/tripetto-block-dropdown){:target="_blank"}, [checkbox](https://www.npmjs.com/package/tripetto-block-checkbox){:target="_blank"} etc.);
- *Condition blocks*: Provide building blocks for conditions (for example evaluate a certain given answer).

#### `Slots`
All data collected through the [runner](../runner/) needs to be stored somewhere. In Tripetto we use `Slots` to do this. Slots are also defined by blocks. For example, if you build a text-input block, you're probably going to retrieve a text value from an input control in your runner implementation. In that case your block should create a slot to store this text value. There are different types of slots, such as a `String`, `Boolean` or `Number`.
