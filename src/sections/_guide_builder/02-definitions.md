---
source: sections/_guide_builder/02-definitions.md
bookmark: definitions
title: Form definitions
---

The builder uses **form definitions** to store the complete structure of your form, including all the properties and settings of the [blocks](../blocks/#introduction) used. A form definition is a so called `JSON` structure stored in a plain `UTF-8` encoded text format. The CLI version of the builder stores these form definitions as plain text files on your disk (you can give them any file name you like). In the library version of the builder it is up to you to decide what to do with the form definition data. Probably you want to store it on some sort of back-end.

The [runner](../runner/) library is used to parse the form definitions into working forms in your website or application.
