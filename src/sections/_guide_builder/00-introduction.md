---
source: sections/_guide_builder/00-introduction.md
title: Builder
code: |
  ``` bash
  # TRY IT NOW!
  # Just open your terminal or command prompt

  # Install the Tripetto builder
  $ npm i tripetto -g

  # Start editing a form
  $ tripetto form.json
  ```
---

[![npm](https://img.shields.io/npm/v/{{ site.npm_packages.builder }}.svg?style=flat-square)](https://www.npmjs.com/package/{{ site.npm_packages.builder }}){:target="_blank"}
[![downloads](https://img.shields.io/npm/dt/{{ site.npm_packages.builder }}.svg?style=flat-square)](https://www.npmjs.com/package/{{ site.npm_packages.builder }}){:target="_blank"}

### Showing you the flow
You use the graphical builder to create and edit smart forms with logic and conditional flows in 2D on a self-organizing drawing board. The builder can run as a stand-alone CLI tool or be tightly integrated into your project. It works in any modern browser and with mouse, touch or pen. The complete structure of a form is stored in a `JSON` format; the [form definition](#definitions).

**The builder comes in two flavors:**
- Stand-alone [CLI tool](#cli) to run the builder locally from your command line;
- JavaScript [library](#library) to integrate the builder into your project.

[![Try the demo](../../images/demo-spaced.svg)](https://example-react-bootstrap.tripetto.com/){:target="_blank"}
[![View the code](../../images/code.svg)](https://gitlab.com/tripetto/examples/react){:target="_blank"}
[![View the package](../../images/npm.svg)](https://www.npmjs.com/package/{{ site.npm_packages.builder }}){:target="_blank"}
