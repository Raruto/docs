---
source: sections/_quickstart/00-welcome.md
title: <punchline />
code: |
  ``` bash
  # TRY IT NOW!
  # Just open your terminal or command prompt

  # Install the Tripetto builder
  $ npm i tripetto -g

  # Start editing a form
  $ tripetto form.json
  ```
---

[![npm](https://img.shields.io/npm/v/{{ site.npm_packages.builder }}.svg?style=flat-square)](https://www.npmjs.com/package/{{ site.npm_packages.builder }}){:target="_blank"}
[![downloads](https://img.shields.io/npm/dt/{{ site.npm_packages.builder }}.svg?style=flat-square)](https://www.npmjs.com/package/{{ site.npm_packages.builder }}){:target="_blank"}
[![twitter](https://img.shields.io/twitter/follow/{{ site.accounts.twitter }}.svg?style=social&label=Follow%20%40tripetto)](https://twitter.com/{{ site.accounts.twitter }}){:target="_blank"}

### Tripetto is a full-fledged form kit
Its graphical [builder](guide/builder/) covers powerful form/survey creation – either stand-alone or as a brandable seamlessly integrated solution. It works in any modern browser; with mouse, touch or pen. The supplementary [runner](guide/runner/) library handles form execution and response collection in any website or application. The UI is truly yours to rock. Also, you can extend both builder and runner with custom [building blocks](guide/blocks/) (e.g. question types).

**There's [multiple ways](#use-cases) you can put Tripetto to work for you.**
- For **stand-alone** form creation and deployment in websites and apps;
- Tightly **integrated** into your own project;
- For powerful **survey creation** (stand-alone or integrated);
- With custom extensions.

🎉 Version 1.0.0 of Tripetto form kit is out. [Check our release notes](https://medium.com/tripetto/a-christmas-present-from-tripetto-b6977a946126){:target="_blank"}.
{: .success }

Not in the mood for reading? Try our code examples right away. We have examples for [React](https://example-react-bootstrap.tripetto.com){:target="_blank"}, [Angular](https://example-angular-bootstrap.tripetto.com){:target="_blank"}, [Material-UI](https://example-react-material-ui.tripetto.com){:target="_blank"}, [Angular Material](https://example-angular-material.tripetto.com){:target="_blank"}, [conversational UI/UX](https://example-react-conversational.tripetto.com){:target="_blank"} and [more](https://gitlab.com/{{ site.accounts.gitlab }}/examples){:target="_blank"}.
{: .success }
