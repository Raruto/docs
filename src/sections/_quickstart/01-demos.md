---
source: sections/_quickstart/01-demos.md
bookmark: demos
title: Demos
---

[![Try the demo](images/demo-spaced.svg)](https://example-react-bootstrap.tripetto.com/){:target="_blank"}
[![Get the code](images/code.svg)](https://gitlab.com/{{ site.accounts.gitlab }}/examples/react){:target="_blank"}

![Screen recording - Showcase](images/screen-recordings/showcase.gif)

Or try some other demos:
- [React + Bootstrap](https://example-react-bootstrap.tripetto.com/){:target="_blank"}
- [React +  Material UI](https://example-react-material-ui.tripetto.com/){:target="_blank"}
- [Angular + Bootstrap](https://example-angular-bootstrap.tripetto.com/){:target="_blank"}
- [Angular + Angular Material](https://example-angular-material.tripetto.com/){:target="_blank"}
- [Conversational UX with React](https://example-react-conversational.tripetto.com/){:target="_blank"}
