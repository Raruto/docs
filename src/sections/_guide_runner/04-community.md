---
source: sections/_guide_runner/04-community.md
bookmark: community
title: Community
---

We hope other enthusiasts will also start to develop runners for Tripetto in the open source domain. We have a special repository where we collect a list of community driven runners and blocks.

[github.com/{{ site.accounts.github }}/community](https://github.com/{{ site.accounts.github }}/community){:target="_blank"}
{: .hyperlink }

#### Add your own runner to the list
If you have created a runner yourself, create a PR and add yours to the list.

This documentation is updated as we continue our work on Tripetto and build a community. Please [let us know](../../support) if you run into issues or need additional information. We’re more than happy to keep you going and also improve the documentation.
{: .warning }
