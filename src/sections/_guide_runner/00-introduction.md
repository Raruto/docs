---
source: sections/_guide_runner/00-introduction.md
title: Runner
code: |
  ``` bash
  # Add the Tripetto runner to your project
  $ npm i tripetto-runner-foundation
  ```
---

[![npm](https://img.shields.io/npm/v/{{ site.npm_packages.runner }}.svg?style=flat-square)](https://www.npmjs.com/package/{{ site.npm_packages.runner }}){:target="_blank"}
[![license](https://img.shields.io/npm/l/{{ site.npm_packages.runner }}.svg?style=flat-square)](https://www.npmjs.com/package/{{ site.npm_packages.runner }}){:target="_blank"}
[![downloads](https://img.shields.io/npm/dt/{{ site.npm_packages.runner }}.svg?style=flat-square)](https://www.npmjs.com/package/{{ site.npm_packages.runner }}){:target="_blank"}

### Making your forms fly
You use the supplementary runner library to handily deploy smart forms in websites and applications. It turns a [form definition](builder/#definitions) (created with the [builder](../builder/)) into an executable program; a finite state machine that handles all the complex logic and response collection during the execution of the form. Apply any UI framework you like. Or pick from the out-of-the-box implementations for [React](https://gitlab.com/{{ site.accounts.gitlab }}/examples/react){:target="_blank"}, [Angular](https://gitlab.com/{{ site.accounts.gitlab }}/examples/angular){:target="_blank"}, [Material-UI](https://gitlab.com/{{ site.accounts.gitlab }}/examples/react-material-ui){:target="_blank"}, [Angular Material](https://gitlab.com/{{ site.accounts.gitlab }}/examples/angular-material){:target="_blank"} and [more](https://gitlab.com/{{ site.accounts.gitlab }}/examples){:target="_blank"}.

**The runner solves a couple of things at once:**

- #### Form parsing
Firstly, the runner takes on the task of parsing the [form definition](../builder/#definitions) from the builder into an executable program. This altogether eliminates the typically complex programming and editing by hand of logic and flows within forms. Once implemented, the runner will autonomously run whatever you create or alter in the builder.

- #### UI freedom
Also, you can implement your own UI and let the runner do the heavy lifting of running the forms. We don’t impose any particular UI framework or library. You decide how it looks. Just wire it up to any UI you like by using the standard DOM-methods of the browser, or by using a library like [React](https://facebook.github.io/react/){:target="_blank"} or framework like [Angular](https://angular.io/){:target="_blank"}.

You may even go commando and make something completely different. For instance, something like an interface to a braille device, optimizing the experience for visually impaired users.

[![Try the demo](../../images/demo-spaced.svg)](https://example-react-bootstrap.tripetto.com/){:target="_blank"}
[![View the code](../../images/code.svg)](https://gitlab.com/tripetto/examples/react){:target="_blank"}
[![Get the package](../../images/npm.svg)](https://www.npmjs.com/package/{{ site.npm_packages.runner }}){:target="_blank"}

For the implementation of the runner we recommend using [TypeScript](https://www.typescriptlang.org/){:target="_blank"}. The runner package includes typings to enable optimal [IntelliSense](https://code.visualstudio.com/docs/builder/intellisense){:target="_blank"} support.
{: .info }

This step-by-step guide for implementing the runner assumes a good understanding of [TypeScript](https://www.typescriptlang.org/){:target="_blank"}, [object-oriented programming](https://www.typescriptlang.org/docs/handbook/classes.html){:target="_blank"} and [webpack](https://webpack.js.org/){:target="_blank"}.
{: .warning }


