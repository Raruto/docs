---
base:
permalink: /
title: Tripetto Documentation - Documentation for smart forms and surveys for developers
custom: true
---

{% assign sections = site.quickstart | sort: "path" %}
{% include sections.html %}
