---
base: ../
permalink: /guide/
source: pages/guide/index.md
title: Guide - Tripetto Documentation
heading: Guide
---

Want to integrate the builder into your own project? Create a runner for response collection? Or create your own building block? Here you will find all the information you need.

### Builder
Use the graphical builder to create and edit smart forms with logic and conditional flows in 2D on a self-organizing drawing board.

[Start with the builder](builder/)
{: .start }

### Runner
Use the runner library to handily deploy smart forms in websites and applications.

[Start with the runner](runner/)
{: .start }

### Blocks
Use the blocks API to create your own form building blocks (e.g. question types).

[Start with blocks](blocks/)
{: .start }
